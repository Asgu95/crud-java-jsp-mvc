<%-- 
    Document   : list
    Created on : 13-nov-2018, 15:54:30
    Author     : User
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="Model.Persona"%>
<%@page import="ModeloDao.PersonaDao"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Personas</h1>
        table
        <table border="1">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>DNI</th>
                    <th>NOMBRE</th>
                    <th>OPCIONES</th>
                </tr>
            </thead>
            <%
                PersonaDao dao = new PersonaDao();
                List<Persona> list = dao.listar();
                Iterator<Persona> iter = list.iterator();
                Persona per = null;
                
                while(iter.hasNext()){
                    per = iter.next();
                
            %>
            <tbody>
                <tr>
                    <td><%=per.getId() %></td>
                    <td><%=per.getDni() %></td>
                    <td><%=per.getNombre() %></td>
                    <td>
                        <a>Editar</a> 
                        <a>Eliminar</a> 
                    </td>
                </tr>
                <% } %>
            </tbody>
        </table>

    </body>
</html>
